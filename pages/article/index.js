import moment from "moment";
import { mapGetters } from "vuex";

export default {
  layout: "layout",
  components: {
    moment
  },
  computed: {
    ...mapGetters({
      articles: "getArticles"
    })
  },
  data() {
    return {
      article: null
    };
  },
  mounted() {
    let foundArticle = this.articles.find(x => x.key == this.$route.params.id);
    if (foundArticle) {
      this.article = foundArticle;
    }
  },
  methods: {
    formattedDate(date) {
      return moment(date).format("Do, MMM YYYY hh:mm:ss");
    }
  }
};
