import moment from "moment";
import { mapGetters } from "vuex";

export default {
  layout: "layout",
  components: {
    moment
  },
  computed: {
    ...mapGetters({
      getSelectedUser: "getSelectedUser"
    })
  },
  data() {
    return {
      newTitle: "",
      newDesc: ""
    };
  },
  methods: {
    clickHandler() {
      let newEntry = {
        title: this.newTitle,
        text: this.newDesc,
        key: Math.floor(Math.random() * 10000 + 100),
        likes: 0,
        author: this.getSelectedUser,
        timeStamp: moment().format()
      };
      this.$store.commit("addArticle", newEntry);
      this.newTitle = "";
      this.newDesc = "";
      this.$router.push({ name: "index" });
    }
  }
};
