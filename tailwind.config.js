const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  theme: {
    extend: {
      screens: {
        xs: { max: "575px" }, // Mobile (iPhone 3 - iPhone XS Max).
        sm: { min: "576px", max: "897px" }, // Mobile (matches max: iPhone 11 Pro Max landscape @ 896px).
        md: { min: "898px", max: "1199px" }, // Tablet (matches max: iPad Pro @ 1112px).
        lg: { min: "1200px" }, // Desktop smallest.
        xl: { min: "1159px" }, // Desktop wide.
        xxl: { min: "1359px" } // Desktop widescreen.
      },
      colors: {
        transparent: "transparent",

        black: "#000",
        white: "#fff",

        primary: "#E9F1FB",
        secondary: "#D4E0F2",
        textPrimary: "#7B89A0",
        buttonPrimary: "#5478FB"
      },
      fontFamily: {
        mono: ["Nunito", ...defaultTheme.fontFamily.mono]
      },
      boxShadow: {
        neumorphic: "12px 12px 24px #c8cfd8, -12px -12px 24px #ffffff",
        ...defaultTheme.boxShadow
      }
    }
  }
};
