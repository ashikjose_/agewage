import Vue from "vue";
import VuexPersistence from "vuex-persist";

Vue.use(new VuexPersistence());
