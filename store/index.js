import VuexPersistence from "vuex-persist";
import Vuex from "vuex";
let vuexLocalStorage = null;

if (process.browser) {
  vuexLocalStorage = new VuexPersistence({
    key: "vuex",
    storage: window.localStorage
  });
}

const store = () => {
  return new Vuex.Store({
    state: {
      userList: [
        {
          firstName: "Sadface",
          lastName: "Pupper",
          profilePic:
            "https://images.pexels.com/photos/406014/pexels-photo-406014.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
          userId: "189HHSaZ"
        },
        {
          firstName: "Mr. Lazy",
          lastName: "Pupper",
          profilePic:
            "https://images.pexels.com/photos/1108099/pexels-photo-1108099.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
          userId: "1asd56Z"
        },
        {
          firstName: "Mr. Pug",
          lastName: "Pupper",
          profilePic:
            "https://images.pexels.com/photos/1851164/pexels-photo-1851164.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
          userId: "67saXS9"
        }
      ],
      selectedUser: {
        firstName: "",
        lastName: "",
        profilePic: "",
        userId: ""
      },
      articles: []
    },
    getters: {
      getUserList: state => {
        return state.userList;
      },
      getSelectedUser: state => {
        return state.selectedUser;
      },
      getSelectedFirstName: state => {
        return state.selectedUser.firstName;
      },
      getSelectedProfilePicture: state => {
        return state.selectedUser.profilePic;
      },
      getArticles: state => {
        return state.articles;
      }
    },
    mutations: {
      addArticle(state, article) {
        state.articles.push(article);
      },
      likeArticle(state, article) {
        let result = state.articles.find(x => x.key === article.key);
        if (result) {
          result.likes += 1;
        }
      },
      selectUser(state, user) {
        state.selectedUser = user;
      }
    },
    //  actions:{...},
    plugins: process.browser ? [vuexLocalStorage.plugin] : []
  });
};

export default store;
