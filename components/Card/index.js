import moment from "moment";

export default {
  props: {
    article: {
      type: Object,
      default: () => {}
    }
  },
  components: {
    moment
  },
  data() {
    return {};
  },
  methods: {
    formattedDate(date) {
      return moment(date).format("Do, MMM YYYY hh:mm:ss");
    },
    clickLikeHandler(article) {
      this.$store.commit("likeArticle", article);
    },
    routeToArticle() {
      this.$router.push({
        name: "article-id",
        params: { id: this.article.key }
      });
    }
  }
};
