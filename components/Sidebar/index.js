import { mapGetters } from "vuex";

export default {
  props: {},
  computed: {
    ...mapGetters({
      getUserList: "getUserList",
      getSelectedFirstName: "getSelectedFirstName",
      getSelectedProfilePicture: "getSelectedProfilePicture"
    })
  },
  components: {},
  data() {
    return {
      showList: false
    };
  },
  created() {
    this.$store.commit("selectUser", this.getUserList[0]);
  },
  methods: {
    showUser() {
      this.showList = true;
    },
    hideUser() {
      this.showList = false;
    },
    changeUser(rowId, event) {
      let result = this.getUserList.find(x => x.userId === event.target.value);
      if (result) {
        this.$store.commit("selectUser", result);
      }
    }
  }
};
